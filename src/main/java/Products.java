import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "product")
public class Products {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    public String name;
    public String category;
    public BigDecimal price;
    public Integer rate;
    public String description;

    @Override
    public String toString() {
        return "Products{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", price=" + price +
                ", rate=" + rate +
                ", description='" + description + '\'' +
                '}';
    }
}
