import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);

        Menu menuWindow = new Menu();
        menuWindow.menu();
        menuWindow.hibernateProductRepository.entityManager.close();

    }

}
